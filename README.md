[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://github.com/20051231/test_vue)

# vue-test

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

You can use any IDE you prefer. We suggest VSCode or Gitpod.

- For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
- [Vuetify](https://vuetifyjs.com/en/)

After you completed the task, please commit and push to your Github and send us the repository url.
